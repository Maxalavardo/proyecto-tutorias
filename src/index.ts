import { app } from "./app";
import "dotenv/config";
import "reflect-metadata";
import typeormCofing from "./lib/typeorm.config";

/* 
 * Inicializando Servidor Express
 */
async function main() {

  /* 
   * INICIANDO CONEXION CON LA BASE DE DATOS
   * ************************************************
   * Se llama a la propidad que contiene la configuracion de la DB y luego se inicializa
   * se usa una promesa para validar que se este estableciendo la conexion con exito
   * en tal caso de que no, por consola se arrojara el error cpor el cual no conecta
   */
  await typeormCofing
    .initialize()
    .then(() => console.info("Database Connected"))
    .catch((error) => console.log(error.message));


  /* 
   * INICIANDO SERVIDOR
   * Se le otorgara el puerto del servidor por el cual este esta funcionando
   * el puerte se configurara en las variables de entorno
   */
  app.listen(process.env.SERVER_PORT, () => {
    console.info(
      `Server is running on http://127.0.0.1:${process.env.SERVER_PORT}/api`
    );
  });
}

main();
