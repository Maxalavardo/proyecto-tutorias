import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('people')
export class Person extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar' })
    firstName: string;

    @Column({ type: 'date' })
    birthday: Date
}
