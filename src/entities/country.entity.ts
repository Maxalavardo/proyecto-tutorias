import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

//  CREANDO ENTIDAD/TABLA COUNTRIES
//  El nombre de la tabla se puede declarar de 2 maneras
//  por defecto usando el nombre de la clase o declarando el nombre dentro 
//  del decorador Entity, en este caso se uso el plural countries
@Entity('countries')
export class Country extends BaseEntity {
    // Para el uso de las propiedades de Typeorm, la clase extendera del BaseEntity

    // Este decorador declara que la prodiedad de la clase llamada id
    // sera primary key en la tabla countries, ademas que se autogenera sola
    @PrimaryGeneratedColumn()
    id: number;

    // El decorador Column es usado para crear las columnas de la tabla
    // dentro del decorador se le pasara un objeto en el cual se le dara los parametros a dicha columna
    // por ejemplo se le declara el tipo varchar, pero puede cambiarse y este ser del tipo el cual se necesite
    // ademas que puede agregarle mas parametros como que esta sea unique, nullable o el tama;o de dicha columna
    // { type: 'varchar', unique: true, nullable: true, length: 100 }
    @Column({ type: 'varchar' })
    name: string;
}
