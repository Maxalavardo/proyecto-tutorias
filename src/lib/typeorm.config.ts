import { DataSource } from 'typeorm';
import 'dotenv/config';

// conexion para ejemplos con typeorm

/* Esta es la configuracion que se usara para la conexion
 * Con la base de datos, exclusivamente sera solo con postgreSQL en este ejemplo,
 * el HOST, PORT, USERNAME, PASSWORD, DATABASE NAME, seran declarados en las variables de entorno
*/
const typeormCofing = new DataSource({
    type: 'postgres',
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    synchronize: true,
    entities: ['src/entities/*.entity{.ts,.js}'],
});
  

export default typeormCofing;