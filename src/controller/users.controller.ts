import { Request, Response } from "express";
import { connection } from "../lib/db_connection";

export const getAll = async (req: Request, res: Response) => {
  const users = await connection.query("SELECT * FROM USERS");

  res.send(users.rows);
};

export const create = async (req: Request, res: Response) => {
  const { name, lastname, dni, birthdate, gender } = req.body;

  try {
    const dni_exist = await connection.query(
      "SELECT dni FROM USERS WHERE dni=$1",
      [dni]
    );

    if (dni_exist) {
      throw new Error(`error ya existe este dni ${dni}`);
    }

    const users = await connection.query(
      `INSERT INTO USERS ("name", "lastname", "dni", "birthdate", "genero") VALUES ($1, $2, $3, $4, $5) RETURNING *;`,
      [name, lastname, dni, birthdate, gender]
    );

    res.send(users.rows[0]);
  } catch (error: any) {
    return error.message;
  }
};
