import { Request, Response } from "express";
import { Country } from "../entities/country.entity";

/* Controlador para las consultas con la base de datos */

// Get All
export const getAll = async (req: Request, res: Response) => {
  const countries = await Country.find();

  // countries guarda la clase countries que a su vez esta misma esta llamando a su metodo find()
  // el cual le pide a la base de datos que traiga todos los registros dentro de la tabla countries
  // esto seria igual a la sentencia SQL -> SELECT * FROM countries;
  res.json(countries);
};

// Create
export const create = async (req: Request, res: Response) => {
  // recibir en el JSON solo la propiedad name
  const { name } = req.body;

  // el uso de los try catch / promesas es para que cuando ocurra un error
  // la aplicacion siga funcionando y no se tenga que reiniciar el servidor
  try {

    // country esta creando una instancia de la clase country
    // la cual esta recibiendo un objeto para que esta sepa cuales son las propiedades
    // las cuales estara recibiendo
    const country = Country.create({ name });

    // Una vez la instancia este creada, se llamara al metodo save()
    // el cual guardara la instancia country que tiene el name que esta recibiendo
    // esto seria igual a la sentencia SQL -> INSERT INTO countries(name) values ('nombre_recibir');
    await Country.save(country);

    /* 
        Manera usando las propiedades de la clase Country
        const country = new Country()
        country.name = name;

        await country.save();
    */

    res.json(country);
  } catch (error: any) {
    return error.message;
  }
};

// Get One
export const getOne = async (req: Request, res: Response) => {
  // el req.params esta diciendo que esta funcion recibira por parte de la ruta
  // un parametro llamado id
  const { id } = req.params;

  // country esta llamando al metodo findOneBy()
  // este metodo pide recibir un objeto con los parametros para poder hacer la consulta
  // esto seria igual a la sentencia SQL -> SELECT * FROM countries WHERE countries.id = id;
  const country = await Country.findOneBy({ id: Number(id) });

  if (!country) {
    // si no exite ningun registro que contenga el id, este retornara
    // un status http 404 que significa que no se encontro lo pedido
    return res.status(404).json({ message: 'Not found' })
  }

  res.json(country);
};

// Update One
export const update = async (req: Request, res: Response) => {
  // el req.params pide el parametro id
    const { id } = req.params;
    // recibir en el JSON solo la propiedad name
    const { name } = req.body;
  
    // se hace una consulta para identificar si existe el country antes de actualizar
    const country = await Country.findOneBy({ id: Number(id) });
  
    if (!country) {
      return res.status(404).json({ message: 'Not found' })
    }

    // el metodo merge debe de recibir 2 parametros
    // el primero es la entidad que se encotro osea el country
    // y el segundo seran las propiedades que se actualizaran
    Country.merge(country, { name });

    // por ultimo este guardara los datos actualizados
    await Country.save(country);
  
    res.json(country);
};


// Delete One
export const remove = async (req: Request, res: Response) => {
    // el req.params pide el parametro id
    const { id } = req.params;
  
    // se hace una consulta para identificar si existe el country antes de actualizar
    const country = await Country.findOneBy({ id: Number(id) });
  
    if (!country) {
      return res.status(404).json({ message: 'Not found' })
    }

    // el metodo delete se encargara de eliminar la entidad segun su id
    // esto seria igual a la sentencia SQL -> DELETE FROM countries WHERE countries.id = id;
    Country.delete(country.id);
  
    res.json({ message: 'country deleted' });
  };
