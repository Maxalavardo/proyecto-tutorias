import { Router } from "express";
import { create, getAll } from "../controller/users.controller";

export const router = Router();

router.get('/', getAll);
router.post('/', create);
