import { Router } from "express";
import { create, getAll, getOne, remove, update } from "../controller/country.controller";

// Iniciando rutas para Country
export const router = Router();

// ruta Get All
router.get('/', getAll);
// ruta Create
router.post('/', create);
// ruta Get One
router.get('/:id', getOne);
// ruta Update One
router.put('/:id', update);
// ruta Delete One
router.delete('/:id', remove);
