import express from 'express';
import { router } from './routes/routes'
import Cors from 'cors';

// Creando propieadad express para configuracion de la app
export const app = express();

// Dando permiso a express para poder recibir JSON en la api
app.use(express.json());

// Dando permisos globales de cors a express para el uso libre de la api
app.use(Cors());

// Importacion de las routas para las consultas de la api
app.use('/api', router);
