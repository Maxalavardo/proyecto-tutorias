## Correr Comando npm i
***

## Crear Archivo .env
***
# Crear archivo .env en ruta raiz al lado del .env.example, copiar y pegar contenido del example

Puedes elegir el puerto que queireas utilizar. Ejemplo 8000
```
# Node.js server configuration
SERVER_PORT=8000

# Database sql configuration
DB_TYPE=postgres
DB_HOST=localhost
DB_USER=example
DB_PASSWORD=234567example
DB_DATABASE=exampleName
DB_PORT=5432
```

## Luego Correr Comando npm run build y luego npm run dev
***
